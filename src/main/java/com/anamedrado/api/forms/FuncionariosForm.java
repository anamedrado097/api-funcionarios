package com.anamedrado.api.forms;

import javax.validation.constraints.Email;

import org.hibernate.validator.constraints.Length;

import com.anamedrado.api.entity.FuncionarioEntity;
import com.anamedrado.api.repository.FuncionarioRepository;
import com.sun.istack.NotNull;

public class FuncionariosForm {

	@NotNull
	@Length(min = 2, max = 30, message = "o nome deve ter entre 2 e 30 caracteres")
	private String nome;
	
	@NotNull
	@Length(min = 2, max = 50, message = "O sobrenome deve ter entre 2 e 50 caracteres")
	private String sobrenome;
	
	@Email(message = "E-mail não valido")
	private String email;
	
	private Integer documento;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSobrenome() {
		return sobrenome;
	}
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getDocumento() {
		return documento;
	}
	public void setDocumento(Integer documento) {
		this.documento = documento;
	}
	
	
	public FuncionarioEntity converter() {
		return new FuncionarioEntity(nome, sobrenome, email, documento);
	}
	public FuncionarioEntity atualizar(Long id, FuncionarioRepository funcionarioRepository) {
		FuncionarioEntity funcionario = funcionarioRepository.getOne(id);
		funcionario.setNome(this.nome);
		funcionario.setSobrenome(this.sobrenome);
		funcionario.setEmail(this.email);
		funcionario.setDocumento(this.documento);
		return funcionario;
	}
}
