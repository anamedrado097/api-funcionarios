package com.anamedrado.api.dto;

import java.util.List;
import java.util.stream.Collectors;

import com.anamedrado.api.entity.FuncionarioEntity;

public class FuncionarioDTO {

	private Long id;
	private String nome;
	private String sobrenome;
	private String email;
	private Integer documento;
	
	public FuncionarioDTO ( FuncionarioEntity funcionario) {
		this.id = funcionario.getId();
		this.nome = funcionario.getNome();
		this.sobrenome = funcionario.getSobrenome();
		this.email = funcionario.getEmail();
		this.documento = funcionario.getDocumento();
	}
	
	public Long getId() {
		return id;
	}
	public String getNome() {
		return nome;
	}
	public String getSobrenome() {
		return sobrenome;
	}
	public String getEmail() {
		return email;
	}
	public Integer getDocumento() {
		return documento;
	}

	public static List<FuncionarioDTO> converter(List<FuncionarioEntity> funcionarios) {
		return funcionarios.stream().map(FuncionarioDTO::new).collect(Collectors.toList());
	}	

}
