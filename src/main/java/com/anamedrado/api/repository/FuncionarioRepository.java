package com.anamedrado.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.anamedrado.api.entity.FuncionarioEntity;

public interface FuncionarioRepository extends JpaRepository<FuncionarioEntity, Long>{

}
