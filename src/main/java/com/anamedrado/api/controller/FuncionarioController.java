package com.anamedrado.api.controller;

import java.net.URI;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.anamedrado.api.dto.FuncionarioDTO;
import com.anamedrado.api.entity.FuncionarioEntity;
import com.anamedrado.api.forms.FuncionariosForm;
import com.anamedrado.api.repository.FuncionarioRepository;

@CrossOrigin
@RestController
@RequestMapping("/funcionarios")
public class FuncionarioController {
	
	
	@Autowired
	private FuncionarioRepository funcionarioRepository;
	
	@CrossOrigin
	@GetMapping
	public List<FuncionarioDTO> list(){
		List<FuncionarioEntity> funcionarios = funcionarioRepository.findAll();
		return FuncionarioDTO.converter(funcionarios);
	}
	
	@CrossOrigin
	@GetMapping("/{id}")
	public FuncionarioDTO listById(@PathVariable Long id) {
		
		FuncionarioEntity funcionario = funcionarioRepository.getOne(id); 
		return new FuncionarioDTO(funcionario);
	}
	
	@CrossOrigin
	@PostMapping
	@Transactional
	public ResponseEntity<FuncionarioDTO> salvar(@RequestBody @Valid FuncionariosForm form, UriComponentsBuilder uriBuilder) {
		
		FuncionarioEntity funcionario = form.converter();
		funcionarioRepository.save(funcionario);
		
		URI uri = uriBuilder.path("/funcionarios/{id}").buildAndExpand(funcionario.getId()).toUri();
		return ResponseEntity.created(uri).body(new FuncionarioDTO(funcionario));
	}
	
	@CrossOrigin
	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<FuncionarioDTO> atualizar(@PathVariable Long id, @RequestBody @Valid FuncionariosForm form){
		FuncionarioEntity funcionario = form.atualizar(id, funcionarioRepository);
	
		return ResponseEntity.ok(new FuncionarioDTO(funcionario));
	}
	
	@CrossOrigin
	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<?> remover(@PathVariable Long id){
		funcionarioRepository.deleteById(id);
		return ResponseEntity.ok().build();
	}

}
